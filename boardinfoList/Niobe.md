---
title: Niobe
permalink: /supported_devices/Niobe
navbar: true
sidebar: false
prev: false
next: false
search: false
article: false
comment: false
editLink: false
date: 2021-10-11 17:56:59
---
# Niobe

<img src="/devices/Niobe/figures/Niobe/6924eeb198d01b9918c2f6b7187898e.jpg">

<img src="/devices/Niobe/figures/Niobe/all.jpg">

## 芯片参数

| 名称  | 参数                                        |
| ----- | ------------------------------------------- |
| Soc   | Hi3861                                      |
| CPU   | 高性能 32 bit 微处理器，最大工作频率 160MHz |
| RAM   | 内嵌 SRAM 352KB                             |
| ROM   | 288KB                                       |
| Flash | 内嵌2MB Flash                               |



## 外设

| 名称         | 参数                                |
| ------------ | ----------------------------------- |
| Wireless     | WiFi: 2.4GHz IEEE 802.11 a/b/g/n/ac |
| UART         | 3路                                 |
| IIC          | 1路                                 |
| SPI          | 1路                                 |
| GPIO         | 13路                                |
| ADC          | 6路                                 |
| power  Soure | DC 5V                               |
|              |                                     |



# 其他

* OpenHarmony 兼容性认证（通过）
* [Software](https://gitee.com/openharmony-sig/devboard_device_talkweb_niobe) 
* [Hardware](https://gitee.com/talkweb_oh/niobe/tree/master/applications/docs/board)
* [Support](https://gitee.com/openharmony-sig/devboard_device_talkweb_niobe/issues)
