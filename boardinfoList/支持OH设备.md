---
title: 支持OpenHarmony设备
permalink: /supported_devices
navbar: true
sidebar: false
prev: false
next: false
search: false
article: false
comment: false
editLink: false
date: 2021-10-16 00:50:12
---
# 支持 OpenHarmony 设备

## 支持设备类型:

-   轻量系统（mini system）
    面向MCU类处理器例如Arm Cortex-M、RISC-V 32位的设备，硬件资源极其有限，支持的设备最小内存为128KiB，可以提供多种轻量级网络协议，轻量级的图形框架，以及丰富的IOT总线读写部件等。可支撑的产品如智能家居领域的连接类模组、传感器设备、穿戴类设备等。

-   小型系统（small system）
    面向应用处理器例如Arm Cortex-A的设备，支持的设备最小内存为1MiB，可以提供更高的安全能力、标准的图形框架、视频编解码的多媒体能力。可支撑的产品如智能家居领域的IP Camera、电子猫眼、路由器以及智慧出行域的行车记录仪等。

-   标准系统（standard system）
    面向应用处理器例如Arm Cortex-A的设备，支持的设备最小内存为128MiB，可以提供增强的交互能力、3D GPU以及硬件合成能力、更多控件以及动效更丰富的图形能力、完整的应用框架。可支撑的产品如高端的冰箱显示屏。

## 设备开发快速入门

- [轻量和小型系统设备开发入门](/pages/00010000)
- [标准系统设备开发入门](/pages/00010100)

## 计划支持 OpenHarmony 设备清单 

**轻量系统**

| 开发板                                                   | 芯片型号        | 兼容性测试 | 资料                                                         |
| -------------------------------------------------------- | --------------- | ---------- | ------------------------------------------------------------ |
| [BearPi_HM_Nano](/supported_devices/BearPi_HM_Nano)      | Hi3561          | 通过       | [Software](https://gitee.com/openharmony/device_bearpi_bearpi_hm_nano)   /  [Hardware](https://gitee.com/bearpi/bearpi-hm_nano/tree/master/applications/BearPi/BearPi-HM_Nano/docs/board) / [Support](https://gitee.com/openharmony/device_bearpi_bearpi_hm_nano/issues) |
| [BK7231X](/supported_devices/BK7231X)                    | BK7231M/BL2028N | 进行中     | [Software](https://gitee.com/openharmony-sig/device_beken ) / [Hardware](https://gitee.com/xxkit/bk7231_for_hilink) / [Support](https://gitee.com/openharmony-sig/device_beken/issues) |
| [BL_HWC_G1](/supported_devices/BL_HWC_G1)                | BL602C          | 进行中     | [Software](https://gitee.com/openharmony-sig/device_bouffalolab) / [Hardware](https://dev.bouffalolab.com/hardware) / [Support](https://zulip.openharmony.cn/#narrow/stream/7-devboard_sig) |
| [GenkiPi](/supported_devices/GenkiPI)                    | Hi3861          | 通过       | [Software](https://gitee.com/openharmony-sig/devboard_device_itcast_genkipi)  / [Support](https://gitee.com/openharmony-sig/devboard_device_itcast_genkipi/issues) |
| [HH_SLNPT100](/supported_devices/HH_SLNPT100)            | W800            | 进行中     | [Software](https://gitee.com/openharmony-sig/device_winnermicro) /  [Hardware](https://gitee.com/hihope_iot/docs/tree/master/Neptune#neptune%E5%BC%80%E5%8F%91%E6%9D%BF%E6%8C%87%E5%8D%97) / [Support](https://gitee.com/openharmony-sig/device_winnermicro/issues) |
| [Niobe](/supported_devices/Niobe)                        | Hi3861          | 通过       | [Software](https://gitee.com/openharmony-sig/devboard_device_talkweb_niobe)  / [Hardware](https://gitee.com/talkweb_oh/niobe/tree/master/applications/docs/board) / [Support](https://gitee.com/openharmony-sig/devboard_device_talkweb_niobe/issues) |
| [Waffle Nano](/supported_devices/Waffle_Nano)            | Hi3861          | 进行中     | [Software](https://gitee.com/openharmony-sig/devboard_waffle_nano) / [Support](https://gitee.com/openharmony-sig/devboard_waffle_nano/issues) |
| [XR806](/supported_devices/XR806)                        | XR806           | 通过       | [Software](https://gitee.com/openharmony-sig/devboard_device_allwinner_xr806) / [Hardware](https://xr806.docs.aw-ol.com/) / [Support](https://gitee.com/openharmony-sig/devboard_device_allwinner_xr806/issues) |
| [GR5515 Starter Kit](/supported_devices/GR5515_Starter_Kit.md) | GR5515RGBD | 通过     | [Software](https://gitee.com/openharmony-sig/device_board_goodix) / [Hardware](https://product.goodix.com/zh/kit/gr5515_starter_kit) / [Support](https://developers.goodix.com/zh/bbs/list?orderType=answer) |



**小型系统**

| 开发板                                | 芯片型号 | 兼容性测试 | 资料                                                         |
| ------------------------------------- | -------- | ---------- | ------------------------------------------------------------ |
| [Halley5](/supported_devices/Halley5) | X2000    | 进行中     | [Software](https://gitee.com/dsqiu/oh-ingenic) /  [Support](https://gitee.com/openharmony-sig/device_ingenic/issues) |



**标准系统**

|  开发板   |  芯片型号  | 兼容性测试  | 资料  |
| ----  |  ----  |  ----  |  ----  |
| [DAYU100](/supported_devices/DAYU100)  | T618 | 进行中 |  [Software](https://gitee.com/openharmony-sig/devboard_device_hihope_build) / [Hardware](https://gitee.com/hi-hope-dayu/doc/tree/master/Hardware) / [Support](https://gitee.com/openharmony-sig/devboard_device_hihope_build/issues) |
| [DAYU200](/supported_devices/DAYU200)  | Rk3568 | 进行中 | [Software](https://gitee.com/openharmony-sig/device_rockchip) / [Hardware](https://gitee.com/hihope-rockchip/doc/tree/master/Hardware) /  [Support](https://gitee.com/openharmony-sig/device_rockchip/issues) |
| [EVB_OH1](/supported_devices/EVB_OH1)  | T507 | 进行中 |  [Software](https://gitee.com/openharmony-sig/devboard_device_allwinner_t507) / [Hardware](https://gitee.com/iotclub/evb_oh1_board.git) / [Support](https://gitee.com/openharmony-sig/devboard_device_allwinner_t507/issues)|
| [RaspberryPi 3Model B](/supported_devices/RPI_3B)  | BCM2837B0 | 进行中 | [Software](https://gitee.com/openharmony-sig/devboard_vendor_rpi3b)  /  [Hardware](https://www.raspberrypi.org/documentation/computers/raspberry-pi.html/) / [Support](https://zulip.openharmony.cn/#narrow/stream/7-devboard_sig/topic/Raspberrypi_Porting) |
| [UnionPi](/supported_devices/UnionPi)  | A311D | 进行中 |  [Software](https://gitee.com/openharmony-sig/device_unionpi.git)  / [Hardware](https://www.aliyundrive.com/s/p6USpMydhWJ) / [Support](https://gitee.com/openharmony-sig/device_unionpi/issues) |

