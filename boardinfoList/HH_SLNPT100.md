---
title: HH-SLNPT100
permalink: /supported_devices/HH_SLNPT100
navbar: true
sidebar: false
prev: false
next: false
search: false
article: false
comment: false
editLink: false
date: 2021-10-11 17:56:59
---


# HH-SLNPT100
  开发板正面：

<img src="/devices/HH_SLNPT100/figure/Neptune/Neptune.png">
  开发板背面：
<img src="/devices/HH_SLNPT100/figure/Neptune/Neptune_back.png">

# 芯片参数


| 名称          | 参数                                                         |
| ------------- | ------------------------------------------------------------ |
| SoC           | WinnerMicro W800芯片32位XT804处理器，工作频率240MHz，内置DSP、浮点运算单元与安全引擎 |
| 操作系统      | 支持HarmonyOS、FreeRTOS                                      |
| 存储空间      | 2MB Flash，288KB RAM                                         |
| UART          | 5路UART高速接口                                              |
| ADC           | 2路16比特SD-ADC，最高采样率1KHz                              |
| I2C           | 1个I2C控制器                                                 |
| GPIO          | 最多支持 18个GPIO                                            |
| PWM           | 5路PWM接口                                                   |
| I2S           | 1路Duplex I2S控制器                                          |
| Wi-Fi         | 支持GB15629.11-2006，IEEE802.11 b/g/n支持 Wi-Fi WMM/WMM-PS/WPA/WPA2/WPS支持 Station、Soft-AP、Soft-AP/Station 功能 |
| Wi-Fi发射功率 | 802.11b：19±2 dBm (\@DSSS 1Mbps) 802.11g：14±2 dBm (\@OFDM 54Mbps) 802.11n：12±2 dBm (@OFDM，MCS7，HT20 |
| Wi-Fi接收参数 | CCK，1 Mbps：-93dBmCCK，11 Mbps：-87dBmOFDM，54 Mbps：-73dBmHT20，MCS7：-71dB |
| BT            | 集成蓝牙基带处理器/协议处理器，支持 BT/BLE 双模工作模式，支持 BT/BLE4.2 协议 |
| 蓝牙发射功率  | BLE功率控制范围：-10 ~ 12dBm，典型值6dBmBT功率控制范围：-10 ~ 8dBm，典型值0dB |
| 蓝牙接收参数  | BLE灵敏度：-94 dBm\@30.8% PERBT灵敏度：-88 dBm\@0.01% BER      |
| 电源管理      | 支持Wi-Fi节能模式功耗管理支持工作、睡眠、待机、关机工作模    |
| 安全特性      | 硬件加密模块：RC4256、AES128、DES/3DES、SHA1/MD5、CRC32、2048 RSA、真随机数发生器 |
| 功耗 (典型值) | 持续发射：240mA\@11b 1Mbps持续接收：95m                       |
| 供电范围      | 供电电压3.0V ~ 3.6V，供电电流 >300mA                         |
| 工作温度      | -20 ℃ ~ 85 ℃                                                 |
| 存储环境      | -40 ℃ ~ 90 ℃，< 90%RH                                        |

  

# 其他

* OpenHarmony 兼容性认证（未过）
* [Software](https://gitee.com/openharmony-sig/device_winnermicro)
* [Hardware](https://gitee.com/hihope_iot/docs/tree/master/Neptune#neptune%E5%BC%80%E5%8F%91%E6%9D%BF%E6%8C%87%E5%8D%97)
* [Support](https://gitee.com/openharmony-sig/device_winnermicro/issues)
