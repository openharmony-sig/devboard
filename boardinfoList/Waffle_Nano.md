---
title: Waffle Nano
permalink: /supported_devices/Waffle_Nano
navbar: true
sidebar: false
prev: false
next: false
search: false
article: false
comment: false
editLink: false
date: 2021-10-11 17:56:59
---
# Waffle Nano 

<img src="/devices/Waffle_Nano/figures/WaffleNano/wafflenano1.png">
<img src="/devices/Waffle_Nano/figures/WaffleNano/wafflenano2.png">

## 芯片参数
- Soc
  
    Hi3861V100

- CPU
  
    高性能 32bit 微处理器，最大工作频率 160MHz

- RAM
  
    内嵌 SRAM 352KB

- ROM
  
    ROM 288KB

- Falsh
  
    内嵌 2MB Flash

## 外设
- Wireless

    1x1 2.4GHz 频段（ch1～ch14）
    支持 IEEE802.11b/g/n 单天线所有的数据速率
    支持标准 20MHz 带宽和 5M/10M 窄带宽

- USB
  
    Type-C USB 供电、UART数据传输

- Display

    240*240 TFT LCD 屏幕

- UART 3个

- I2C 2个

- SPI 2个

- GPIO 15个

- SDIO 1个

- I2S 1个

- ADC输入 7路

- PWM 6路

## 其他

- OpenHarmony 兼容性认证（进行中）
- [Software](https://gitee.com/openharmony-sig/devboard_waffle_nano)
- [Support]([support@blackwalnut.tech](https://gitee.com/openharmony-sig/devboard_waffle_nano/issues))
