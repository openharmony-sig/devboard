---
title: DAYU100
permalink: /supported_devices/DAYU100
navbar: true
sidebar: false
prev: false
next: false
search: false
article: false
comment: false
editLink: false
date: 2021-10-11 17:56:59
---
# DAYU100

<img src="/devices/DAYU100/figure/DAYU100/DAYU100.png">


# 芯片参数

* Soc                                  

     Unisoc T618                                             

* CPU                                  

     双核 A75 at 2.0GHz & 六核 A55 at 2.0GHz

* GPU                                

     Mali-G52 3EE2-Core

* RAM                                   

  4GB DDR4

* ROM                                  

  64GB EMMC

   

# 外设



* Wireless                         

  WiFi: 5 GHz & 2.4GHz IEEE 802.11 a/b/g/n/ac 

  Bluetooth® v5.0

   External Antenna
* GNSS

  支持 GPS/GLONASS/Beidou/Galileo

* USB                                  

  Host: 1x type C, 2.0 high-speed 

  OTG: 1x type C, 2.0 high-speed

* Display                           

   MIPI DSI

* Audio

  Speaker X1
  3.5mm 耳机接口

* Camera

  Front Camera X1
  back  Camera X1

* SIM Card Slot                        X2

* UART                                 X4

* I2C                                  X7

* SPI                                  X2

* SDIO                                 X1

* Power Source                       DC 3.6V~ DC 4.2V 3A  

  

# 其他

* OpenHarmony 兼容性认证 进行中
* [Software](https://gitee.com/openharmony-sig/devboard_device_hihope_build)                             
* [Hardware](https://gitee.com/hi-hope-dayu/doc/tree/master/Hardware)
* [Support](https://gitee.com/openharmony-sig/devboard_device_hihope_build/issues)
