# GR5515 Starter Kit 

<img src="/images/devices/GR5515-SK-BASIC/figures/front.png">

# 芯片参数

|  名称  |            参数            |
| ------ | ------------------------- |
| Soc    | GR5515RGBD                |
| CPU    | ARM Cortex-M4F up to 64Mhz |
| RAM    | 256KB SRAM                |
| Memory | 1MB Flash                 |


# 外设

|  名称  |                                                                                                                                                                                     参数                                                                                                                                                                                     |
| ----- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| BLE   | 集成控制器和主机层的低功耗蓝牙5.1收发器<br/>支持数据传输速率：1 Mbps、2 Mbps、LR（500 kbps、125 kbps）<br/>TX发射功率：-20 dBm ～ +7 dBm<br/>-97 dBm接收灵敏度（1 Mbps模式下）<br/>-93 dBm接收灵敏度（2 Mbps模式下）<br/>-99.5 dBm接收灵敏度（LR 500 kbps模式下）<br/>-103 dBm接收灵敏度（LR 125 kbps模式下）<br/>TX发射功耗：3.05 mA @ 0 dBm，1 Mbps<br/>RX接收功耗：3.9 mA @ 1 Mbps |
| GPIO  | 最多支持39个GPIO                                                                                                                                                                                                                                                                                                                                                             |
| UART  | 2路                                                                                                                                                                                                                                                                                                                                                                          |
| I2C   | 2路                                                                                                                                                                                                                                                                                                                                                                          |
| I2S   | 2路(1路I2S Master接口,1路I2S Slave接口)                                                                                                                                                                                                                                                                                                                                      |
| QSPI  | 2路                                                                                                                                                                                                                                                                                                                                                                          |
| SPI   | 2路(1路SPI Master接口,1路SPI Slave接口)                                                                                                                                                                                                                                                                                                                                      |
| PWM   | 6路                                                                                                                                                                                                                                                                                                                                                                          |
| ADC   | 8通道13位ADC,最高1 Msps,支持单端和差分输入                                                                                                                                                                                                                                                                                                                                    |
| Power | 片内DC-DC 1.7V-3.8V                                                                                                                                                                                                                                                                                                                                                          |


# 其他

* OpenHarmony 兼容性认证 （通过）
* [software](https://gitee.com/openharmony-sig/device_board_goodix)
* [hardware](https://www.sekorm.com/product/756747.html)
* [Support](https://gitee.com/openharmony-sig/device_soc_goodix/issues)