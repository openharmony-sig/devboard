# 仓名

* [简介](#section1)
* [目录]()
* [相关仓]()

# 简介<a name="section1"></a>

该仓用来维护统计sig-devboard下正在做OpenHarmony移植的开发板，包括：计划移植、正在移植、移植完成的三类开发板

# 目录

```
├── boardinfoList             # 开发板信息
├── boardShowTemplate         # 开发板资料模板
├── figures                   # 开发板图片
├── README.md                 # 仓描述文档
```

# 说明 

[开发板移植进度表](
https://docs.qq.com/sheet/DYmZ1RmhEZ1RVa0to)

[开发板获取链接](./boardinfoList/开发板获取链接.md)

[开发板列表](./boardinfoList/支持OH设备.md)

# 相关仓

* 无

